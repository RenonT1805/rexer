package rexer_test

import (
	"regexp"
	"testing"

	"gitlab.com/RenonT1805/rexer"
)

func TestLexer(t *testing.T) {
	type params struct {
		symbols   []rexer.Symbol
		source    string
		identType int
	}
	tests := []struct {
		name            string
		params          params
		conditionTokens []*rexer.Token
	}{
		{
			name: "Empty",
			params: params{
				symbols:   nil,
				source:    "",
				identType: 0,
			},
			conditionTokens: []*rexer.Token{
				nil,
			},
		},
		{
			name: "One token",
			params: params{
				symbols:   nil,
				source:    "test",
				identType: 0,
			},
			conditionTokens: []*rexer.Token{
				{Literal: "test", TokenType: 0},
				nil,
			},
		},
		{
			name: "Multi token",
			params: params{
				symbols: []rexer.Symbol{
					rexer.NewPatternSymbol(-1, []string{" ", "\t", "\r", "\n"}),
					rexer.NewPatternSymbol(1, []string{"test", "fuga"}),
				},
				source:    "test hoge fuga",
				identType: 0,
			},
			conditionTokens: []*rexer.Token{
				{Literal: "test", TokenType: 1},
				{Literal: "hoge", TokenType: 0},
				{Literal: "fuga", TokenType: 1},
				nil,
			},
		},
		{
			name: "Golang source lexing sample",
			params: params{
				symbols: []rexer.Symbol{
					rexer.NewPatternSymbol(-1, []string{" ", "\t", "\r", "\n"}),
					rexer.NewRegexSymbol(1, []*regexp.Regexp{regexp.MustCompile(`".+"`)}),
					rexer.NewPatternSymbol(2, []string{"package", "import", "func"}),
					rexer.NewPatternSymbol(3, []string{".", "(", ")", "{", "}"}),
				},
				source: `
					package main

					import "fmt"

					func main() {
						fmt.Printf("Hello World\n")
					}
				`,
				identType: 0,
			},
			conditionTokens: []*rexer.Token{
				{Literal: `package`, TokenType: 2},
				{Literal: `main`, TokenType: 0},
				{Literal: `import`, TokenType: 2},
				{Literal: `"fmt"`, TokenType: 1},
				{Literal: `func`, TokenType: 2},
				{Literal: `main`, TokenType: 0},
				{Literal: `(`, TokenType: 3},
				{Literal: `)`, TokenType: 3},
				{Literal: `{`, TokenType: 3},
				{Literal: `fmt`, TokenType: 0},
				{Literal: `.`, TokenType: 3},
				{Literal: `Printf`, TokenType: 0},
				{Literal: `(`, TokenType: 3},
				{Literal: `"Hello World\n"`, TokenType: 1},
				{Literal: `)`, TokenType: 3},
				{Literal: `}`, TokenType: 3},
				nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			lexer := rexer.NewLexer(tt.params.source)
			lexer.DefaultTokenType = tt.params.identType
			lexer.SkipTokenType = -1
			lexer.Symbols = tt.params.symbols
			resultTokens := []*rexer.Token{}
			for {
				token := lexer.NextToken()
				resultTokens = append(resultTokens, token)
				if token == nil {
					break
				}
			}
			if len(resultTokens) != len(tt.conditionTokens) {
				t.Errorf("result length = %v, condition length %v", len(resultTokens), len(tt.conditionTokens))
				return
			}

			for i := range resultTokens {
				if resultTokens[i] == nil || tt.conditionTokens[i] == nil {
					if resultTokens[i] == tt.conditionTokens[i] {
						continue
					} else {
						t.Errorf("result = %v, condition %v", resultTokens[i], tt.conditionTokens[i])
						return
					}
				}
				if resultTokens[i].Literal != tt.conditionTokens[i].Literal ||
					resultTokens[i].TokenType != tt.conditionTokens[i].TokenType {
					t.Errorf("result = %v, condition %v", *resultTokens[i], *tt.conditionTokens[i])
				}
			}
		})
	}
}
