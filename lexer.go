package rexer

import (
	"regexp"
)

type Lexer struct {
	// トークン化の進捗行を表します。
	Line int

	// トークン化の進捗列を表します。
	Col int

	// トークン化するためのSymbolデータです。
	// Symbolデータは、文章を単語に分割するための単語帳のようなものです。
	Symbols []Symbol

	// トークンの取得をスキップするトークンタイプです。
	// 例えば、空白をトークンとして取得したくない場合、読み飛ばす為に利用できます。
	SkipTokenType int

	// Symbolsに当てはまらないトークンはこのトークンタイプが割り当てられます。
	DefaultTokenType int

	lines []string
}

type Token struct {
	Literal   string
	TokenType int
}

// NewLexer は、字句解析を行うためのLexerを生成します。
// srcに指定された文字列から、NextTokenを使用してトークンを取り出します。
func NewLexer(src string) *Lexer {
	return &Lexer{
		Line:          0,
		Col:           0,
		SkipTokenType: -1,
		lines:         regexp.MustCompile("\r\n|\n").Split(src, -1),
	}
}

// NextToken は、呼ばれる度にStrから次のトークンを取得します。
// SkipTokenTypeに設定されたタイプのトークンが取得された場合、
// そのトークンは読み捨てられ、次のトークンが返されます。
// 終端まで読まれた場合、nilを返します。
func (l *Lexer) NextToken() *Token {
	if len(l.lines) <= l.Line {
		return nil
	}

	if len(l.lines[l.Line]) <= l.Col {
		l.Line++
		l.Col = 0
		return l.NextToken()
	}

	var (
		str        string = l.lines[l.Line][l.Col:]
		max        int
		min        int = len(str)
		symbolType int = l.DefaultTokenType
	)
	for _, symbol := range l.Symbols {
		p, length := symbol.Find(str)
		if p != -1 && p < min {
			min = p
		}
		if p == 0 && max < length {
			max = length
			symbolType = symbol.GetType()
		}
	}

	if max == 0 {
		max = min
	}

	token := str[:max]
	l.Col += max

	if symbolType == l.SkipTokenType {
		return l.NextToken()
	}

	return &Token{
		Literal:   token,
		TokenType: symbolType,
	}
}
