package rexer_test

import (
	"regexp"
	"testing"

	"gitlab.com/RenonT1805/rexer"
)

func TestNewPatternSymbol(t *testing.T) {
	type params struct {
		patterns   []string
		symbolType int
		source     string
	}
	tests := []struct {
		name                string
		params              params
		conditionIndex      int
		conditionLength     int
		conditionSymbolType int
	}{
		{
			name: "Find symbol",
			params: params{
				patterns: []string{
					"test",
				},
				symbolType: 1,
				source:     "This is the test text.",
			},
			conditionIndex:      12,
			conditionLength:     4,
			conditionSymbolType: 1,
		},
		{
			name: "Symbol not found",
			params: params{
				patterns: []string{
					"lexer",
				},
				symbolType: 2,
				source:     "This is the test text.",
			},
			conditionIndex:      -1,
			conditionLength:     0,
			conditionSymbolType: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			symbol := rexer.NewPatternSymbol(tt.params.symbolType, tt.params.patterns)
			symbolType := symbol.GetType()
			if tt.conditionSymbolType != symbolType {
				t.Errorf("result symbol type = %v, condition symbol type %v", symbolType, tt.conditionSymbolType)
			}
			p, length := symbol.Find(tt.params.source)
			if p != tt.conditionIndex {
				t.Errorf("result index = %v, condition index %v", p, tt.conditionIndex)
			}
			if length != tt.conditionLength {
				t.Errorf("result length = %v, condition length %v", length, tt.conditionLength)
			}
		})
	}
}

func TestNewRegexSymbol(t *testing.T) {
	type params struct {
		exprs      []*regexp.Regexp
		symbolType int
		source     string
	}
	tests := []struct {
		name                string
		params              params
		conditionIndex      int
		conditionLength     int
		conditionSymbolType int
	}{
		{
			name: "Find symbol",
			params: params{
				exprs: []*regexp.Regexp{
					regexp.MustCompile("test"),
				},
				symbolType: 1,
				source:     "This is the test text.",
			},
			conditionIndex:      12,
			conditionLength:     4,
			conditionSymbolType: 1,
		},
		{
			name: "Find using regex",
			params: params{
				exprs: []*regexp.Regexp{
					regexp.MustCompile(`".+"`),
				},
				symbolType: 1,
				source:     `fmt.Println("hello, world")`,
			},
			conditionIndex:      12,
			conditionLength:     14,
			conditionSymbolType: 1,
		},
		{
			name: "Symbol not found",
			params: params{
				exprs: []*regexp.Regexp{
					regexp.MustCompile("lexer"),
				},
				symbolType: 2,
				source:     "This is the test text.",
			},
			conditionIndex:      -1,
			conditionLength:     0,
			conditionSymbolType: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			symbol := rexer.NewRegexSymbol(tt.params.symbolType, tt.params.exprs)

			symbolType := symbol.GetType()
			if tt.conditionSymbolType != symbolType {
				t.Errorf("result symbol type = %v, condition symbol type %v", symbolType, tt.conditionSymbolType)
			}
			p, length := symbol.Find(tt.params.source)
			if p != tt.conditionIndex {
				t.Errorf("result index = %v, condition index %v", p, tt.conditionIndex)
			}
			if length != tt.conditionLength {
				t.Errorf("result length = %v, condition length %v", length, tt.conditionLength)
			}
		})
	}
}
