package rexer

import (
	"regexp"
	"strings"
)

// Symbol は、文字列をトークン化するためのパターンを表します。
type Symbol interface {
	// 生成時に設定されたトークンタイプを返します
	GetType() int

	// 与えられた文字列から設定されたパターンに一致する箇所を検索し、
	// indexと長さを返します。
	Find(string) (int, int)
}

type RegrexSymbol struct {
	regex []*regexp.Regexp
	t     int
}

func (s RegrexSymbol) GetType() int {
	return s.t
}

func (s RegrexSymbol) Find(str string) (int, int) {
	min := -1
	length := -1
	for _, r := range s.regex {
		if result := r.FindIndex([]byte(str)); result != nil {
			if result[0] < min || min == -1 {
				min = result[0]
				length = result[1]
			}
		}
	}

	if min == -1 {
		return -1, 0
	}

	length = length - min
	return min, length
}

// NewRegexSymbol は、正規表現を利用するSymbolです。
// 例えば、NewRegexSymbol(StringType, []*regexp.Regexp{regexp.MustCompile(`".+"`)})
// とした場合、"" で囲まれた部分がStringTypeのSymbolとして判断されます。
func NewRegexSymbol(symbolType int, exprs []*regexp.Regexp) Symbol {
	symbol := RegrexSymbol{
		t:     symbolType,
		regex: exprs,
	}
	return symbol
}

type PatternSymbol struct {
	pattern []string
	t       int
}

func (s PatternSymbol) GetType() int {
	return s.t
}

func (s PatternSymbol) Find(str string) (int, int) {
	min := -1
	length := -1
	for _, item := range s.pattern {
		if i := strings.Index(str, item); i != -1 {
			if i < min || min == -1 {
				min = i
				length = len(item)
			}
		}
	}

	if min == -1 {
		return -1, 0
	}

	return min, length
}

// NewPatternSymbol は、単純なパターンマッチを利用するSymbolです。
// 例えば、NewPatternSymbol(TestType, []string{`test`})
// とした場合、文中のtestの文字列の部分がTestTypeのSymbolとして判断されます。
func NewPatternSymbol(symbolType int, pattern []string) Symbol {
	return PatternSymbol{
		t:       symbolType,
		pattern: pattern,
	}
}
